import java.util.*;
public class Board{

    public int[][] p;
	public int[][] special;
    private int blackCount;
    private int whiteCount;
    private int emptyCount;

    private static final int NONE  = 0;
    private static final int WHITE = -1;
    private static final int BLACK = 1;
    private static final int EDGE  = 2;

    public Board(){
	p = new int[10][10];
	special = new int[10][10];

        for( int i = 0; i < 10; i++ ){
        	for( int j = 0; j < 10; j++){
        		p[ i ][ j ] = NONE;
        		special[i][j] = NONE;
        	}
        }
        for( int i = 0; i < 10; i++ ){
            p[ i ][ 0 ] = EDGE;
            p[ i ][ 9 ] = EDGE;
            p[ 0 ][ i ] = EDGE;
            p[ 9 ][ i ] = EDGE;
        }
        p[5][5]=p[4][4]=WHITE;
        p[4][5]=p[5][4]=BLACK;
	updateInfo();
    }
	public Board(String s){
	p = new int[10][10];
	special = new int[10][10];
        for( int i = 0; i < 10; i++ ){
        	for( int j = 0; j < 10; j++){
                special[i][j] = NONE;
        		if(s.charAt(i*10+j) == '0')p[ i ][ j ] = NONE;
        		else if(s.charAt(i*10+j) == '1')p[ i ][ j ] = WHITE;
        		if(s.charAt(i*10+j) == '2')p[ i ][ j ] = BLACK;
        	}
        }
        for( int i = 0; i < 10; i++ ){
            p[ i ][ 0 ] = EDGE;
            p[ i ][ 9 ] = EDGE;
            p[ 0 ][ i ] = EDGE;
            p[ 9 ][ i ] = EDGE;
        }

	updateInfo();
    }
    public Board clone(){
	Board copy = new Board();
	for(int i = 0;i < 10;i++)
		for(int j = 0;j<10;j++)
			copy.p[i][j] = this.p[i][j];
	copy.blackCount = this.blackCount;
	copy.whiteCount = this.whiteCount;
	copy.emptyCount = this.emptyCount;
	return copy;
    }
    public ArrayList<Move> getValidMove( int color ){
    	ArrayList<Move> v = new ArrayList <Move>();
        for( int i = 1; i < 9; i++ )
        {
            for( int j = 1 ; j < 9; j++ )
                if( isValidMove( color, i, j ) )
                {
                	Move s = new Move();
                	s.x = i;
                	s.y = j;
                    v.add(s);
                }
        }
        return v;
    }
    public boolean isValidMove(  int color, int row, int col ){
        if( p[ row ][ col ] != NONE )
            return false;
        for( int dirR = -1; dirR <= 1; dirR++ )
            for( int dirC = -1; dirC <= 1; dirC++ )
                if( !( dirR == 0 && dirC == 0 ) && isDirectionValid( color, row, col, dirR, dirC ) )
                    return true;
        return false;
    }
    public int isAnyValidMove( int color ){
        int counter = 0;
        for( int i = 1; i < 9; i++ )
        {
            for( int j = 1 ; j < 9; j++ )
                if( isValidMove( color, i, j ) )
                {
                    counter++;
                }
        }
        return counter;
    }
    public boolean isDirectionValid( int color, int row, int col, int dirR, int dirC ){
        int enemy = -color;
        int r = row + dirR;
        int c = col + dirC;
        while( p[ r ][ c ] != EDGE && p[ r ][ c ] == enemy )
        {
            r += dirR;
            c += dirC;
        }
        if( ( p[ r ][ c ] == EDGE ) || ( r - dirR == row && c - dirC == col ) || ( p[ r ][ c ] != color ) )
            return false;

        return true;
    }
    public void makeMove( int color, int row, int col ){
        p[ row ][ col ] = color;
		int enemy = -color;
		int dirR, dirC;
		int r, c;
		for ( dirR = -1; dirR <= 1; dirR++ )
		    for ( dirC = -1; dirC <= 1; dirC++ )
				if ( !(dirR == 0 && dirC == 0) && isDirectionValid( color, row, col, dirR, dirC ))
				{
					r = row + dirR;
					c = col + dirC;
					while( p[ r ][ c ] == enemy)
					{
						p[ r ][ c ] = color;
                        r += dirR;
						c += dirC;
					}
				}
		updateInfo();
    }
    private void updateInfo(){
		blackCount = 0;
		whiteCount = 0;
		emptyCount = 0;

        for( int i = 1; i < 9; i++ ){
            for( int j = 1; j < 9; j++ ){
                if ( p[ i ][ j ] == NONE)
                    emptyCount++;
                else if ( p[ i ][ j ] == BLACK )
                    blackCount++;
                else if ( p[ i ][ j ] == WHITE )
                    whiteCount++;
            }
        }
    }
    public int getBlackCount(){
        return blackCount;
    }
    public int getWhiteCount(){
        return whiteCount;
    }
    public int getEmptyCount(){
        return emptyCount;
    }
};
