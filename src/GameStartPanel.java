import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GameStartPanel extends JLayeredPane implements ActionListener
{
	private JButton start;
	private JLabel bgLabel;
	protected ImageIcon bg;
	private MainLayeredPane mlp;
	public GameStartPanel(MainLayeredPane mlp)
	{
		this.mlp = mlp;
		start = new JButton("START");
		start.addActionListener(this);
		bg = new ImageIcon("images/attack_on_oop_logo.png");
		bgLabel = new JLabel(bg);
		add(bgLabel, new Integer(0));
		add(start, new Integer(1));
		start.setLocation(380, 350);
		start.setOpaque(true);
		start.setSize(100, 30);
		bgLabel.setLocation(0, 0);
		bgLabel.setSize(800, 600);
	}

	public void actionPerformed(ActionEvent e)
	{
		mlp.startMap();
	}


}
