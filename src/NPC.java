import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class NPC extends JLabel implements MouseListener
{
	protected mainCharacter mainCH;
	protected int x, y;
	protected MainLayeredPane mlp;

	public NPC(String body_path, mainCharacter mainCH, int x, int y, MainLayeredPane mlp)
	{
		super((new ImageIcon(body_path)));
		this.mainCH = mainCH;
		this.mlp = mlp;
		this.x = x;
		this.y = y;
		setLocation(x, y);
		setSize(25, 80);
		addMouseListener(this);
	}
	protected void activate()
	{
		mlp.startShop();
	}
	public void mousePressed(MouseEvent e)
	{
	}

	public void mouseClicked(MouseEvent e)
	{
		if((mainCH.getPosX()-x)*(mainCH.getPosX()-x)+(mainCH.getPosY()-y)*(mainCH.getPosY()-y)<= 6400)
		{
			activate();
		}
	}

	public void mouseReleased(MouseEvent e)
	{
	}

	public void mouseEntered(MouseEvent e)
	{
	}

	public void mouseExited(MouseEvent e)
	{
	}
}
