
public class titan extends character
{
	public final static int BIG = 4, ARMOR = 3, WEIRD = 2, NORMAL = 1;
	private int type;
	private String str_board;
	private Board board;
	public titan(String head_path, String body_path, String name, int hp, int maxHP, int mp,int maxMP, int atk, int def, int luk, int lv, int exp, int posX, int  posY, int money)
	{
		super(head_path, body_path);
		setName(name);
		setATTR(hp, maxHP, mp, maxMP, atk, def, luk, lv, exp, posX, posY, money);
	}
	public static titan getTitan(int type, int x, int y)
	{
		titan t;
		if(type == 1)
		{
			t = new titan("images/head1.jpg", "images/titan2.png", "Titan", 60, 60, 10, 10, 7, 5, 3, 3, 150, x, y, 50);
			t.str_board = "0000000000"+
					"0000000000"+
					"0000000000"+
					"0000000000"+
					"0000120000"+
					"0000210000"+
					"0000000000"+
					"0000000000"+
					"0000000000"+
					"0000000000";
			t.lbl.setOpaque(false);
			t.lbl.setLocation(x, y);
			t.lbl.setSize(90, 233);
		t.type = type;
		return t;
		}
		else if(type == 2)
		{
			t = new titan("images/head1.jpg", "images/titan2.png", "Weird Titan", 100, 100, 30, 30, 15, 8, 40, 15, 700, x, y, 100);
		t.str_board = "0000000000"+
					"0000000000"+
					"0010000200"+
					"0001002000"+
					"0000120000"+
					"0000210000"+
					"0002001000"+
					"0020000100"+
					"0000000000"+
					"0000000000";
		t.lbl.setOpaque(false);
			t.lbl.setLocation(x, y);
			t.lbl.setSize(90, 233);
		t.type = type;
		return t;
		}
		else if(type == 3)
		{
			t = new titan("images/head1.jpg", "images/titan2.png", "Armor Titan", 300, 300, 70, 70, 20, 20, 20, 40, 2000, x, y, 900);
			t.str_board = "0000000000"+
						"0000000000"+
						"0000000000"+
						"0000000000"+
						"0000120000"+
						"0000210000"+
						"0000000000"+
						"0000000000"+
						"0000000000"+
						"0000000000";
			t.lbl.setOpaque(false);
			t.lbl.setLocation(x, y);
			t.lbl.setSize(90, 233);
		t.type = type;
		return t;
		}
		else if(type == 4)
		{
			t = new titan("images/head1.jpg", "images/large_titan.png", "BIG Titan", 500, 500, 100, 100, 70, 40, 30, 80, 3500, x, y, 1500);
			t.str_board = "0000000000"+
						"0000000000"+
						"0000000000"+
						"0000000000"+
						"0000120000"+
						"0000210000"+
						"0000000000"+
						"0000000000"+
						"0000000000"+
						"0000000000";
			t.lbl.setOpaque(false);
			t.lbl.setLocation(x, y);
			t.lbl.setSize(277, 535);
		t.type = type;
		return t;
		}
		return null;
	}
	public int getType()
	{
		return type;
	}

	public Board getBoard()
	{
		return new Board(str_board);
	}
}
