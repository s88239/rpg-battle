import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class map extends JLayeredPane
{
	private ImageIcon bg, ch, tiicon;
	private JLabel bgLabel, chLabel, tiLabel;
	private JLayeredPane map_jlp;
	private canvasLabel lineLabel;
	private MainLayeredPane mlp;
	private map this_map;
	private MoveListener m;
	private AttackListener atkl;
	private int nowX, nowY, x, y;
	private mainCharacter mainCH;
	private Battle battle;
	private int ok, lining;
	private double ddx, ddy;
	private titan target;
	private titan[] ti;
	private NPC npc;
	private int num_titan;

	public map()
	{
		super();
		bg = new ImageIcon("images/bgg.png");
		bgLabel = new JLabel(bg);
		lineLabel = new canvasLabel();
		ok = 0;
		lining = 0;
		ddx = 0.0;
		ddy = 0.0;
		num_titan = 0;
		this_map = this;
		ti = new titan[20];
		for(int j = 0; j<20;j++)
		{
			ti[j] = null;
		}
	}
	public void setmainCH(mainCharacter mainCH)
	{
		this.mainCH = mainCH;
		nowX = mainCH.getPosX();
		nowY = mainCH.getPosY();
	}
	public void startMap(MainLayeredPane mlp/*, int stage*/)
	{
		this.mlp = mlp;
		//chLabel = new JLabel(mainCH.body);
		/*----titan test*/
		DeployTitan();
		//tiicon = new ImageIcon("images/tt.png");
		//tiLabel = new JLabel(tiicon);
		/*--------------*/

		add(bgLabel, new Integer(0));
		mlp.add(mainCH.lbl, new Integer(1), 0);
		mlp.add(lineLabel, new Integer(1), 1);
		mlp.add(this, new Integer(0));
		npc = new NPC("images/vendor.png", mainCH, 300, 450, mlp);
		add(npc, new Integer(2));

		nowX = mainCH.getPosX();
		nowY = mainCH.getPosY();

		setLocation(400-nowX, 300-nowY);
		setSize(4000, 2000);
		mainCH.lbl.setOpaque(false);
		mainCH.lbl.setLocation(400, 300);
		mainCH.lbl.setSize(30, 80);
		bgLabel.setLocation(0, 0);
		bgLabel.setSize(4000, 2000);
		lineLabel.setLocation(0, 0);
		lineLabel.setSize(800, 600);
		lineLabel.setOpaque(false);


		m = new MoveListener(this);
		bgLabel.addMouseListener(m);
		bgLabel.repaint();
		repaint();

		ti[0] = titan.getTitan(4, 3200, 10);
		add(ti[0].lbl, new Integer(1), 0);
		atkl = new AttackListener(this, 0);
		ti[0].lbl.addMouseListener(atkl);
	}
	public void replay()
	{
		mainCH.setPosX(400);
		mainCH.setPosY(300);
		nowX = 400;
		nowY = 300;
		setLocation(0, 0);
		mainCH.setHP(1);
		for(int j = 1; j< 20;j++)
		{
			ti[j] = null;
		}
		DeployTitan();
		ti[0] = titan.getTitan(4, 3200, 10);
		add(ti[0].lbl, new Integer(1), 0);
		atkl = new AttackListener(this, 0);
		ti[0].lbl.addMouseListener(atkl);
	}
	public void DeployTitan()
	{
		int x, y;
		for(int j = 1; j< 20;j++)
		{
			if(ti[j] == null)
			{
				while(true)
				{
					x = (int)(Math.random()*2400+600);
					y = (int)(Math.random()*1700);
					for(int k = 0; k < 20;k++)
					{
						if(j == k || ti[k] == null)continue;
						if(Math.abs(x - ti[k].getPosX())<100 && Math.abs(y - ti[k].getPosY()) < 200)
						{
							x = -1;
							break;
						}
					}
					if(x < 0)continue;
					else break;
				}
				ti[j] = titan.getTitan((int)(Math.random()*3+1), x, y);
				add(ti[j].lbl, new Integer(1), j);
				atkl = new AttackListener(this, j);
				ti[j].lbl.addMouseListener(atkl);
			}
		}
	}
	public void RespawnTitan()
	{
		int k = -1;
		for(int j = 0;j<20;j++)
		{
			if(ti[j] == target)
			{
				k = j;
				break;
			}
		}
		if(k > 0)
		{
			remove(getIndexOf(ti[k].lbl));
			ti[k] = null;
			DeployTitan();
		}
	}

	public void moved(int xx, int yy)
	{
		//System.out.printf("%d %d\n", xx, yy);
		this.x = xx;
		this.y = yy;
		this.x -= 10; //the center of character
		this.y -= 25;
		ActionListener al = new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int dx, dy;
				double dddx, dddy;
				if(ok == 1)
				{
				if(x > nowX && y > nowY)
				{
					if((x-nowX) > (y-nowY))
					{
						dx = ((x - nowX)/(y-nowY));
						if(dx > 2)dx = 2;
						dy = 1;
					}
					else
					{
						dx = 1;
						dy = (y-nowY)/(x-nowX);
						if(dy > 2)dy = 2;
					}
				}
				else if(x > nowX && y < nowY)
				{
					if((x-nowX) > (nowY - y))
					{
						dx = (x-nowX)/(nowY - y);
						if(dx > 2)dx = 2;
						dy = -1;
					}
					else
					{
						dx = 1;
						dy = (y-nowY)/(x-nowX);
						if(dy < -2)dy = -2;
					}
				}
				else if(x < nowX && y > nowY)
				{
					if((nowX-x) > (y-nowY))
					{
						dy = 1;
						dx = (x-nowX)/(y-nowY);
						if(dx < -2)dx = -2;
					}
					else
					{
						dx = -1;
						dy = (y-nowY)/(nowX-x);
						if(dy > 2)dy = 2;
					}
				}
				else if(x < nowX && y < nowY)
				{
					if((nowX-x) > (nowY-y))
					{
						dx = (x-nowX)/(nowY-y);
						if(dx < -2)dx = -2;
						dy = -1;
					}
					else
					{
						dx = -1;
						dy = (y-nowY)/(nowX-x);
						if(dy< -2)dy = -2;
					}
				}
				else if(x == nowX)
				{
					dx = 0;
					if((y - nowY) >= 2) dy = 2;
					else if((y - nowY) <= -2)dy = -2;
					else if(y == nowY) dy = 0;
					else
					{
						if(y > nowY) dy = 1;
						else dy = -1;
					}
				}
				else
				{
					dy = 0;
					if((x - nowX)>=2) dx = 2;
					else if((x - nowX)<=-2) dx = -2;
					else if(x == nowX) dx = 0;
					else
					{
						if(x > nowX) dx = 1;
						else dx = -1;
					}
				}
					if(dx == 0 && dy == 0)
					{
						ok = 2;
					}
					Point p = this_map.getLocation();
					this_map.setLocation(p.x-dx, p.y-dy);
					lineLabel.drawLine(410, 325, 410+(x-nowX), 325+(y-nowY));
					nowX += dx;
					nowY += dy;
					mainCH.setPosX(nowX);
					mainCH.setPosY(nowY);
			}
			else if(ok == 0)
			{
				if(lining == 0)
				{
					if(x>nowX) dx = x - nowX;
					else dx = nowX - x;
					if(y>nowY)dy = y - nowY;
					else dy = nowY-y;
					if(dx > dy)
					{
						if(dy == 0)ddx = ((x-nowX)>0)? 1.0: -1.0;
						else ddx = (double)(x-nowX)/(double)dy;

						if(y > nowY) ddy = 1.0;
						else if(y < nowY) ddy = -1.0;
						else ddy = 0.0;

						lineLabel.setS(dy);
					}
					else
					{
						if(dx == 0) ddy = ((y - nowY)>0)? 1.0: -1.0;
						else ddy = (double)(y-nowY)/(double)dx;

						if(x>nowX) ddx = 1.0;
						else if(x < nowX) ddx = -1.0;
						else ddx = 0.0;
						lineLabel.setS(dx);
					}
					lineLabel.drawLine(410, 325, 410+ddx, 325+ddy);
					lining = 1;
				}
				else
				{
					dddx = lineLabel.getgoalX();
					dddy = lineLabel.getgoalY();
					lineLabel.drawLine(410, 325, dddx+ddx, dddy+ddy);
					if(lineLabel.addCount() == 1)
					{
						mainCH.changeIcon(1);
						repaint();
						ok = 1;
						lining = 0;
					}
				}
			}
			else
			{
				ok = 0;
				mainCH.changeIcon(0);
				repaint();
				((Timer)e.getSource()).stop();
			}
			}
		};
		Timer time = new Timer(10, al);
		time.start();

	}
	public void move_attack(int i, int tx, int ty)
	{
		target = ti[i];
		this.x = ti[i].getPosX() + tx;
		this.y = ti[i].getPosY() + ty;
		this.x -= 10; //the center of character
		this.y -= 25;
		ActionListener al = new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int dx, dy;
				double dddx, dddy;
				if(ok == 1)
				{
				if(x > nowX && y > nowY)
				{
					if((x-nowX) > (y-nowY))
					{
						dx = ((x - nowX)/(y-nowY));
						if(dx > 2)dx = 2;
						dy = 1;
					}
					else
					{
						dx = 1;
						dy = (y-nowY)/(x-nowX);
						if(dy > 2)dy = 2;
					}
				}
				else if(x > nowX && y < nowY)
				{
					if((x-nowX) > (nowY - y))
					{
						dx = (x-nowX)/(nowY - y);
						if(dx > 2)dx = 2;
						dy = -1;
					}
					else
					{
						dx = 1;
						dy = (y-nowY)/(x-nowX);
						if(dy < -2)dy = -2;
					}
				}
				else if(x < nowX && y > nowY)
				{
					if((nowX-x) > (y-nowY))
					{
						dy = 1;
						dx = (x-nowX)/(y-nowY);
						if(dx < -2)dx = -2;
					}
					else
					{
						dx = -1;
						dy = (y-nowY)/(nowX-x);
						if(dy > 2)dy = 2;
					}
				}
				else if(x < nowX && y < nowY)
				{
					if((nowX-x) > (nowY-y))
					{
						dx = (x-nowX)/(nowY-y);
						if(dx < -2)dx = -2;
						dy = -1;
					}
					else
					{
						dx = -1;
						dy = (y-nowY)/(nowX-x);
						if(dy< -2)dy = -2;
					}
				}
				else if(x == nowX)
				{
					dx = 0;
					if((y - nowY) >= 2) dy = 2;
					else if((y - nowY) <= -2)dy = -2;
					else if(y == nowY) dy = 0;
					else
					{
						if(y > nowY) dy = 1;
						else dy = -1;
					}
				}
				else
				{
					dy = 0;
					if((x - nowX)>=2) dx = 2;
					else if((x - nowX)<=-2) dx = -2;
					else if(x == nowX) dx = 0;
					else
					{
						if(x > nowX) dx = 1;
						else dx = -1;
					}
				}
					if(dx == 0 && dy == 0)
					{
						ok = 2;
					}
					Point p = this_map.getLocation();
					this_map.setLocation(p.x-dx, p.y-dy);
					lineLabel.drawLine(410, 325, 410+(x-nowX), 325+(y-nowY));
					nowX += dx;
					nowY += dy;
					mainCH.setPosX(nowX);
					mainCH.setPosY(nowY);
			}
			else if(ok == 0)
			{
				if(lining == 0)
				{
					if(x>nowX) dx = x - nowX;
					else dx = nowX - x;
					if(y>nowY)dy = y - nowY;
					else dy = nowY-y;
					if(dx > dy)
					{
						ddx = (double)(x-nowX)/(double)dy;
						if(y >= nowY) ddy = 1.0;
						else ddy = -1.0;

						lineLabel.setS(dy);
					}
					else
					{
						ddy = (double)(y-nowY)/(double)dx;
						if(x>=nowX) ddx = 1.0;
						else ddx = -1.0;
						lineLabel.setS(dx);
					}
					lineLabel.drawLine(410, 325, 410+ddx, 325+ddy);
					lining = 1;
				}
				else
				{
					dddx = lineLabel.getgoalX();
					dddy = lineLabel.getgoalY();
					lineLabel.drawLine(410, 325, dddx+ddx, dddy+ddy);
					if(lineLabel.addCount() == 1)
					{
						mainCH.changeIcon(1);
						ok = 1;
						lining = 0;
					}
				}
			}
			else if(ok == 2)
			{
                battle = new Battle(mainCH, target, mlp);
				mainCH.changeIcon(0);
                ok = 0;
				//this_map.removeAll();
				mlp.startBattle(battle);

				((Timer)e.getSource()).stop();
			}
			}
		};
		Timer time = new Timer(10, al);
		time.start();
	}
}


