import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MainLayeredPane extends JLayeredPane
{
	private map m;
	private Battle nowBattle;
	private GameStartPanel gsp;
	private EndBattlePanel end;
	private mainCharacter mainCH;
	//private JButton b;
	public MainLayeredPane(map m, mainCharacter mainCH)
	{
		super();
		this.m = m;
		this.mainCH = mainCH;
		/*b = new JButton("map");
		b.setLocation(0, 0);
		add(b, new Integer(4));
		b.setSize(100, 30);
		b.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				EndBattle(1, 0);
			}
		});*/
		nowBattle = null;
		end = null;
		setLocation(0, 0);
		setSize(4000, 2000);
	}
	public void startGame()
	{
		gsp = new GameStartPanel(this);
	//	remove(getIndexOf(m));
		add(gsp, new Integer(3));
		gsp.setSize(800, 600);
		gsp.setLocation(0, 0);
		m.setmainCH(mainCH);
		m.startMap(this);
	}
	public void startMap()
	{
		mainCH.setHP(mainCH.getMaxHP());
		mainCH.setMP(mainCH.getMaxMP());
		remove(getIndexOf(gsp));
		repaint();
	}
	public void startBattle(Battle battle)
	{
		//remove(getIndexOf(m));
		repaint();
		nowBattle = battle;
		add(battle, new Integer(3), 1);
		battle.setLocation(0, 0);
		battle.repaint();
	}
	public void startShop()
	{
		Shop shop = new Shop(mainCH, this);
		add(shop, new Integer(3), 0);
		shop.setLocation(0, 0);
		shop.repaint();
		repaint();
	}
	public void EndBattle(int wl, int up, titan ti)
	{

		if(wl == 1)
		{
			m.RespawnTitan();
				end = (up>0)? new LevelUpPanel(this, mainCH, ti, up) : new VictoryPanel(this, ti);
		}
		else if(wl == 0) end = new DefeatPanel(this, ti);
		else end = new EscapePanel(this);

		//remove(getIndexOf(nowBattle));
		//repaint();
		if(wl == 0)
		{
			m.replay();
		}
		nowBattle = null;

		add(end, new Integer(3), 0);
		end.moved();
	}

	public void BackToMap()
	{
		Component[] comp = getComponentsInLayer(3);
		for(int i = 0;i< comp.length;i++)
			remove(getIndexOf(comp[i]));
		repaint();
		//m.startMap(this);
		//add(m, new Integer(0));
	}
	public void BacktoMenu()
	{
		Component[] comp = getComponentsInLayer(3);
		for(int i = 0;i< comp.length;i++)
			remove(getIndexOf(comp[i]));

		add(gsp, new Integer(3), 0);
		repaint();
	}
}

