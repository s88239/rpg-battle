import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class EndBattlePanel extends JLayeredPane
{
	protected ImageIcon bg;
	protected MainLayeredPane mlp;
	protected JLabel bgLabel;

	public EndBattlePanel(MainLayeredPane mlp)
	{
		this.mlp = mlp;
		bg = new ImageIcon("images/win.png");
		bgLabel = new JLabel(bg);
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		bg.paintIcon(this, g, 0, 0);
	}

	public void setBG()
	{
		bgLabel = new JLabel(bg);
		add(bgLabel, new Integer(0));
		bgLabel.setLocation(0, 0);
		bgLabel.setSize(800, 600);

	}

	public void moved()
	{
		ActionListener al = new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				Point p = getLocation();
				if(p.y == 0)((Timer)e.getSource()).stop();

				else setLocation(p.x, p.y+15);
			}
		};
		Timer time = new Timer(25, al);
		time.start();
	}

}

class VictoryPanel extends EndBattlePanel implements ActionListener
{
	private JButton but;
	private JPanel jp;
	private JLabel lbl;

	public VictoryPanel(MainLayeredPane mlpl, titan ti)
	{
		super(mlpl);
		bg = new ImageIcon("images/win.png");
		setBG();
		setLocation(0, -600);
		setSize(800, 600);
		jp = new JPanel(new GridLayout(2, 2));
		jp.add(lbl = new JLabel("EXP: "));
		lbl.setFont(new Font("Serif", Font.BOLD, 36));
		lbl.setForeground(Color.white);
		jp.add(lbl = new JLabel(Integer.toString(ti.getEXP())));
		lbl.setFont(new Font("Serif", Font.BOLD, 48));
		lbl.setForeground(Color.white);
		jp.add(lbl = new JLabel("Money: "));
		lbl.setFont(new Font("Serif", Font.BOLD, 36));
		lbl.setForeground(Color.white);
		jp.add(lbl = new JLabel(Integer.toString(ti.getMoney())));
		lbl.setFont(new Font("Serif", Font.BOLD, 48));
		lbl.setForeground(Color.white);
		add(jp, new Integer(1));
		jp.setLocation(200, 150);
		jp.setSize(400, 100);
		jp.setOpaque(false);
		but = new JButton("Return to map");
		add(but, new Integer(2));
		but.setLocation(380, 550);
		but.setSize(200, 30);
		but.addActionListener(this);
		repaint();
	}

	public void actionPerformed(ActionEvent e)
	{
		mlp.BackToMap();
	}
}

class DefeatPanel extends EndBattlePanel implements ActionListener
{
	private JButton but, exit_but;
	private JPanel jp;

	public DefeatPanel(MainLayeredPane mlpl, titan ti)
	{
		super(mlpl);
		bg = new ImageIcon("images/lose.png");
		setLocation(0, -600);
		setSize(800, 600);
		setBG();
		jp = new JPanel(new GridLayout(1, 2));
		but = new JButton("Return to map");
		exit_but = new JButton("Return to main menu");
		jp.add(but);
		jp.add(exit_but);
		add(jp, new Integer(1));
		jp.setLocation(250, 550);
		jp.setSize(300, 30);
		jp.setOpaque(false);
		but.addActionListener(this);
		exit_but.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				mlp.BacktoMenu();
			}
		});
		repaint();
	}

	public void actionPerformed(ActionEvent e)
	{
		mlp.BackToMap();
	}
}

class LevelUpPanel extends EndBattlePanel implements ActionListener
{
	private JButton but, atk_but, def_but, luk_but;
	private mainCharacter mainCH;
	private JPanel jp;
	private JLabel lbl;
	private int up;
	private MainLayeredPane mlp;

	public LevelUpPanel(MainLayeredPane mlpl, mainCharacter mainCH, titan ti, int up)
	{
		super(mlpl);
		this.mlp = mlpl;
		this.up = up;
		this.mainCH = mainCH;
		bg = new ImageIcon("images/win.png");
		setLocation(0, -600);
		setSize(800, 600);
		setBG();
		jp = new JPanel(new GridLayout(2, 2));
		jp.add(lbl = new JLabel("EXP: "));
		lbl.setFont(new Font("Serif", Font.BOLD, 36));
		lbl.setForeground(Color.white);
		jp.add(lbl = new JLabel(Integer.toString(ti.getEXP())));
		lbl.setFont(new Font("Serif", Font.BOLD, 48));
		lbl.setForeground(Color.white);
		jp.add(lbl = new JLabel("Money: "));
		lbl.setFont(new Font("Serif", Font.BOLD, 36));
		lbl.setForeground(Color.white);
		jp.add(lbl = new JLabel(Integer.toString(ti.getMoney())));
		lbl.setFont(new Font("Serif", Font.BOLD, 48));
		lbl.setForeground(Color.white);
		add(jp, new Integer(1));
		jp.setLocation(200, 150);
		jp.setSize(400, 100);
		jp.setOpaque(false);
		but = new JButton("Return to map");
		add(but, new Integer(2));
		but.setLocation(380, 550);
		but.setSize(100, 30);
		atk_but = new JButton("+ATK");
		def_but = new JButton("+DEF");
		luk_but = new JButton("+LUK");
		add(atk_but, new Integer(3), 0);
		add(def_but, new Integer(3), 1);
		add(luk_but, new Integer(3), 2);
		atk_but.setLocation(400, 450);
		atk_but.setSize(80, 30);
		atk_but.setActionCommand("atk");
		atk_but.addActionListener(this);
		def_but.setLocation(500, 450);
		def_but.setSize(80, 30);
		def_but.setActionCommand("def");
		def_but.addActionListener(this);
		luk_but.setLocation(600, 450);
		luk_but.setSize(80, 30);
		luk_but.setActionCommand("luk");
		luk_but.addActionListener(this);
		add(lbl = (new JLabel(Integer.toString(up))), new Integer(4));
		lbl.setLocation(300, 450);
		lbl.setSize(80, 80);
		lbl.setFont(new Font("Serif", Font.BOLD, 48));
		lbl.setForeground(Color.white);
		but.addActionListener(new ActionListener()
		{
            public void actionPerformed(ActionEvent e)
            {
                mlp.BackToMap();
            }
		});
		repaint();
	}

	public void actionPerformed(ActionEvent e)
	{
		String s = e.getActionCommand();
		if(up <= 0)return;
		if(s.equals("atk"))
		{
            mainCH.setATK(mainCH.getATK()+1);
		}
		else if(s.equals("def"))
		{
            mainCH.setDEF(mainCH.getDEF()+1);
		}
		else
		{
            mainCH.setLUK(mainCH.getLUK()+1);
		}
		up--;
		lbl.setText(Integer.toString(up));
	}
}

class EscapePanel extends EndBattlePanel implements ActionListener
{
	private JButton but;
	public EscapePanel(MainLayeredPane mlpl)
	{
		super(mlpl);
		bg = new ImageIcon("images/escape.jpg");
		setLocation(0, -600);
		setSize(800, 600);
		setBG();
		but = new JButton("Return to map");
		add(but, new Integer(1));
		but.setLocation(280, 550);
		but.setSize(80, 30);
		but.setOpaque(false);
		but.addActionListener(this);
		repaint();
	}

	public void actionPerformed(ActionEvent e)
	{
		mlp.BackToMap();
	}
}
