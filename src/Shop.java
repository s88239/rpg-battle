import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Shop extends JLayeredPane implements ActionListener
{
	Good[] goods;
	JButton[] buy;
	JButton exit;
	JLabel bg, mon;
	JLabel[] num;
	mainCharacter mainCH;
	private JPanel jp, chjp;
	private Item[] items;
	private MainLayeredPane mlp;
	public Shop(mainCharacter mainCH, MainLayeredPane mlpl)
	{
		this.mainCH = mainCH;
		this.mlp = mlpl;
		items = mainCH.getItem();
		num = new JLabel[9];
		exit = new JButton("Exit");
		bg = new JLabel(new ImageIcon("images/frame.jpg"));
		add(bg, new Integer(0));
		bg.setLocation(0, 0);
		bg.setSize(800, 600);
		jp = new JPanel(new GridLayout(9, 3));
        chjp = new JPanel(new GridLayout(10, 2));
		goods = new Good[9];
		goods[0] = new Good(new RedWater(30));
		goods[1] = new Good(new CrimsonWater(50));
		goods[2] = new Good(new BlueWater(40));
		goods[3] = new Good(new SoyaBeanMilk(60));
		goods[4] = new Good(new Whetstone(70));
		goods[5] = new Good(new Testosterone(70));
		goods[6] = new Good(new Lottery(60));
		goods[7] = new Good(new Bomb(120));
		goods[8] = new Good(new NeckCrosshair(5000));
		buy = new JButton[9];
		for(int i = 0; i< 9; i++)
		{
			buy[i] = new JButton("Buy");
			buy[i].setActionCommand(Integer.toString(i));
			buy[i].addActionListener(this);
			num[i] = new JLabel(Integer.toString(items[i].getNum()));
			jp.add(goods[i]);
			jp.add(new JLabel("$"+ Integer.toString(goods[i].getNum())));
			jp.add(buy[i]);
			chjp.add(new JLabel(items[i].getName()));
			chjp.add(num[i]);
		}
		chjp.add(new JLabel("Money: "));
		mon = new JLabel("    "+Integer.toString(mainCH.getMoney()));
		chjp.add(mon);
		add(jp, new Integer(1));
		add(chjp, new Integer(2));
		add(exit, new Integer(3));
		jp.setLocation(100, 50);
		jp.setSize(300, 400);
		chjp.setLocation(500, 50);
		chjp.setSize(200,400);
		exit.setLocation(700, 550);
		exit.setSize(80, 30);
		exit.addActionListener(new ActionListener()
		{
            public void actionPerformed(ActionEvent e)
            {
                mlp.BackToMap();
            }
		});
		setLocation(0, 0);
		setSize(800, 600);
	}
	public void actionPerformed(ActionEvent e)
	{
		int i = Integer.parseInt(e.getActionCommand());
		if(mainCH.getMoney() >= goods[i].getNum())
		{
			mainCH.setMoney(mainCH.getMoney() - goods[i].getNum());
			items[i].setNum(items[i].getNum()+1);
			updateInfo();
		}
	}
	private void updateInfo()
	{
        for(int i = 0; i< 9; i++)
		{
			num[i].setText(Integer.toString(items[i].getNum()));
		}

		mon.setText("    "+Integer.toString(mainCH.getMoney()));

		chjp.repaint();
		repaint();
	}

}
