
public abstract class Item
{
	protected int mp, num;
	protected String name, info, show;
	//protected ImageIcon icon;
	public String getStatus()
	{
		return show;
	}
	public String getName()
	{
		return name;
	}
	public String getInfo()
	{
		return info;
	}
	public int getNum()
	{
		return num;
	}
	public void setNum(int num)
	{
		this.num = num;
	}
	public void Explode(titan t, Battle battle)
	{
	}
	abstract public int use(mainCharacter mainCH, titan ti, Battle battle);
}

class RedWater extends Item
{
	public RedWater(int num)
	{
		this.num = num;
		mp = 10;
		name = "Red Water";
		info = "HP + 20, consume MP " + mp;
		show = "Using Red Water";
	}
	public int use(mainCharacter mainCH, titan ti, Battle battle)
	{
		if(mainCH.getMP() < mp)
		{
			show = "You don't have enough MP\n";
			return -1;
		}
		battle.updateInfo(mainCH, -1, mainCH.subMP(mp));
		num--;
		battle.updateInfo( mainCH , mainCH.subHP(-20) , -1 ); // hp +20
		show = mainCH.getName() + " uses Red Water, HP + 20\n";
		return 1;
	}
}

class CrimsonWater extends Item
{
	public CrimsonWater(int num)
	{
		this.num = num;
		mp = 20;
		name = "Crimson Water";
		info = "HP + 50, consume MP " + mp;
	}
	public int use(mainCharacter mainCH, titan ti, Battle battle)
	{
		if(mainCH.getMP() < mp)
		{
			show = "You don't have enough MP\n";
			return -1;
		}
		battle.updateInfo(mainCH, -1, mainCH.subMP(mp));
		num--;
		battle.updateInfo( mainCH , mainCH.subHP(-50) , -1 ); // hp +50
		show = mainCH.getName() + " uses Crimson Water, HP + 50\n";
		return 1;
	}
}
class BlueWater extends Item
{
	public BlueWater(int num)
	{
		this.num = num;
		mp = 0;
		name = "Blue Water";
		info = "MP + 20, consume MP " + mp;
	}
	public int use(mainCharacter mainCH, titan ti, Battle battle)
	{
		if(mainCH.getMP() < mp)
		{
			show = "You don't have enough MP\n";
			return -1;
		}
		battle.updateInfo(mainCH, -1, mainCH.subMP(mp));
		num--;
		battle.updateInfo( mainCH, -1, mainCH.subMP(-20) ); // mp +20
		show = mainCH.getName() + " uses Blue Water, MP + 20\n";
		return 1;
	}
}
class SoyaBeanMilk extends Item
{
	public SoyaBeanMilk(int num)
	{
		this.num = num;
		mp = 70;
		name = "Soya Bean Milk";
		info = "Change your color to the titan's, consume MP " + mp;
	}
	public int use(mainCharacter mainCH, titan ti, Battle battle)
	{
		if(mainCH.getMP() < mp)
		{
			show = "You don't have enough MP\n";
			return -1;
		}
		battle.updateInfo(mainCH, -1, mainCH.subMP(mp));
		num--;
		battle.changeColor(); // change color
		show = mainCH.getName() + " uses Soya Bean Milk, exchange color\n";
		return 1;
	}
}
class Whetstone extends Item
{
	public Whetstone(int num)
	{
		this.num = num;
		mp = 80;
		name = "Whetstone";
		info = "Sharpen your weapon, ATK + 1, consume MP " + mp;
	}
	public int use(mainCharacter mainCH, titan ti, Battle battle)
	{
		if(mainCH.getMP() < mp)
		{
			show = "You don't have enough MP\n";
			return -1;
		}
		battle.updateInfo(mainCH, -1, mainCH.subMP(mp));
		num--;
		mainCH.setATK(mainCH.getATK()+1);
		battle.updateAbi( false, 4, mainCH.getATK() ); // update the Ability GUI
		show = mainCH.getName() + " uses Whetstone, ATK + 1. ATK is " + mainCH.getATK() + " now!\n";
		return 1;
	}
}
class Testosterone extends Item
{
	public Testosterone(int num)
	{
		this.num = num;
		mp = 80;
		name = "Testosterone";
		info = "A kind of hormone, which can make DEF + 1, consume MP " + mp;
	}
	public int use(mainCharacter mainCH, titan ti, Battle battle)
	{
		if(mainCH.getMP() < mp)
		{
			show = "You don't have enough MP\n";
			return -1;
		}
		battle.updateInfo(mainCH, -1, mainCH.subMP(mp));
		num--;
		mainCH.setDEF(mainCH.getDEF()+1);
		battle.updateAbi( false, 5, mainCH.getDEF() ); // update the Ability GUI
		show = mainCH.getName() + " uses Testosterone, DEF + 1. DEF is " + mainCH.getDEF() + " now!\n";
		return 1;
	}
}
class Lottery extends Item
{
	public Lottery(int num)
	{
		this.num = num;
		mp = 70;
		name = "Lottery";
		info = "It seems that you can win the first prize, LUK + 1, consume MP " + mp;
	}
	public int use(mainCharacter mainCH, titan ti, Battle battle)
	{
		if(mainCH.getMP() < mp)
		{
			show = "You don't have enough MP\n";
			return -1;
		}
		battle.updateInfo(mainCH, -1, mainCH.subMP(mp));
		num--;
		mainCH.setLUK(mainCH.getLUK()+1);
		battle.updateAbi( false, 6, mainCH.getLUK() ); // update the Ability GUI
		show = mainCH.getName() + " uses Lottery, LUK + 1. LUK is " + mainCH.getLUK() + " now!\n";
		return 1;
	}
}
class Bomb extends Item
{
	public Bomb(int num)
	{
		this.num = num;
		mp = 40;
		name = "Bomb";
		info = "Planted on the titan. If explodes, causes damage on titan. Consume MP " + mp;
	}
	public int use(mainCharacter mainCH, titan ti, Battle battle)
	{
		if(mainCH.getMP() < mp)
		{
			show = "You don't have enough MP\n";
			return -1;
		}
		battle.updateInfo(mainCH, -1, mainCH.subMP(mp));
		num--;
		battle.setSpecialChess();
		show = mainCH.getName() + " uses Bomb\n";
		return 1;
	}
	public void Explode(titan t, Battle battle)
	{
		show = "Bomb explode! Cause damage " + t.getHP()/2 + " on titan!\n";
		battle.updateInfo(t, t.subHP(t.getHP()/2), -1);
	}
}
class NeckCrosshair extends Item
{
	public NeckCrosshair(int num)
	{
		this.num = num;
		mp = 100;
		name = "Neck Crosshair";
		info = "Target the neck of titan. Attack its neck to let it lose HP every run, consume MP "+mp;
	}
	public int use(mainCharacter mainCH, titan ti, Battle battle)
	{
		if(mainCH.getMP() < mp)
		{
			show = "You don't have enough MP\n";
			return -1;
		}
		battle.updateInfo(mainCH, -1, mainCH.subMP(mp));
		num--;
		battle.isToxic = true;
		show = mainCH.getName() + " uses Neck Crosshair\n";
		return 1;
	}
	public void Explode(titan t, Battle battle){
		int lossHP = 10;
		show = "Attack titan's neck! Titan lose HP " + lossHP + "\n";
		battle.updateInfo( t, t.subHP( lossHP ), -1);
	}
}
