import java.awt.event.*;
public class MoveListener implements MouseListener
{
	private map m;

	public MoveListener(map m)
	{
		this.m = m;
	}

	public void mousePressed(MouseEvent e)
	{
		//System.out.printf("click\n");
	}

	public void mouseClicked(MouseEvent e)
	{
		m.moved(e.getX(),e.getY());
	}

	public void mouseReleased(MouseEvent e)
	{
	}

	public void mouseEntered(MouseEvent e)
	{
	}

	public void mouseExited(MouseEvent e)
	{
	}
}

