import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;
import java.util.*;
public class Battle extends JPanel implements Runnable{
	public static final int ITEM_ROW = 3;
	public static final int ITEM_COL = 3;
	private mainCharacter player;
	private titan titan;
	public MainLayeredPane map;
	private int color, currentItem;//1 black, -1 white
	private JPanel displayPanel, infoPanel, insPanel, chessPanel, titanPanel, playerPanel, itemPanel, itemShowing, abilityPanel, currentPanel, itemInfo;
	private JButton[][] chessButton, itemButton;
	private JLabel[][] chessIcon, itemNum, charAbi;
	private JTextArea showing, itemMessage;
	private JTextField titanHP, titanMP, playerHP, playerMP;
	private JScrollPane scroll;
	private ImageIcon white, black;
	private GridBagConstraints gridCons;
	private Board currentBoard;
	private Item[] item;
	private boolean isText, isSpecialChess;
	public boolean isToxic;
	private Move tStep, pStep;
	public Battle(mainCharacter player, titan titan, MainLayeredPane map){
		this.player = player;
		this.titan = titan;
		this.map = map;
		setSize(800, 570);
		setLayout(new BorderLayout());

		currentBoard = titan.getBoard();// current chessboard
		color = 1;//black chess
		
		white = new ImageIcon("images/white.png");// white chess image
		black = new ImageIcon("images/black.png");// black chess image

		tStep = new Move();
		pStep = new Move();

		/*GridBagConstraints is the parameter for GridBagLayout*/
		gridCons = new GridBagConstraints();
		gridCons.gridwidth = 1;
		gridCons.gridheight = 1;
		gridCons.weightx = 0;
		gridCons.weighty = 0;
		gridCons.fill = GridBagConstraints.NONE;
		gridCons.anchor = GridBagConstraints.CENTER;

		isSpecialChess = false;
		isToxic = false;

		/*displayPanel setting*/
		displayPanel = new JPanel();
		displayPanel.setLayout(new BorderLayout());
		//displayPanel.setBackground(Color.GRAY);

		chessPanel = new ImagePane("images/chessboard.jpg");// to store chessboard in GUI form
		chessPanel.setLayout(new GridBagLayout());
		//chessPanel.setBackground(Color.LIGHT_GRAY);
		/*create a chessboard*/
		chessButton = new JButton[10][10];
		chessIcon = new JLabel[10][10];
		initChessboard();//initialize the chessboard
		displayPanel.add(chessPanel, BorderLayout.CENTER);

		isText = true;// TextArea work
		showing = new JTextArea(8,10);
		showing.setEditable(false);
		scroll = new JScrollPane( showing , ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setPreferredSize(new Dimension(100, 150));
		displayPanel.add(scroll, BorderLayout.SOUTH);

		add(displayPanel, BorderLayout.CENTER);

		/*infoPanel: character information*/
		infoPanel = new JPanel();// to store character information
		infoPanel.setLayout(new BorderLayout());
		infoPanel.setBackground(Color.PINK);
		/*titan information*/
		titanPanel = new JPanel();//store the information of titan
		titanHP = new JTextField( titan.getHP() + "/" + titan.getMaxHP() );
                titanMP = new JTextField( titan.getMP() + "/" + titan.getMaxMP() );
		initCharInfo( titan, titanPanel, titanHP, titanMP);
		infoPanel.add(titanPanel, BorderLayout.NORTH);
		/*player information*/
		playerPanel = new JPanel();//store the information of player
                playerHP = new JTextField( player.getHP() + "/" + player.getMaxHP() );
                playerMP = new JTextField( player.getMP() + "/" + player.getMaxMP() );
		initCharInfo( player, playerPanel, playerHP, playerMP);
                infoPanel.add(playerPanel, BorderLayout.SOUTH);
		add(infoPanel, BorderLayout.EAST);

		/*choose instruction*/
		insPanel = new JPanel();
		insPanel.setLayout(new FlowLayout());
		//insPanel.setBackground(Color.YELLOW);
		JButton defend, item, ability, runaway;
		defend = new JButton("defend");
		defend.addActionListener(new DefendAction());
		item = new JButton("item");
		item.addActionListener(new ItemAction());
		initItem();
		ability = new JButton("ability");
		ability.addActionListener(new AbilityAction());
		runaway = new JButton("run away");
		runaway.addActionListener(new RunawayAction());
		/*create a panel to show the ability for both character*/
		abilityPanel = new JPanel();
		//abilityPanel.setBackground(Color.YELLOW);
		abilityPanel.setPreferredSize(new Dimension(100, 150));
		abilityPanel.setLayout(new BorderLayout());
		charAbi = new JLabel[2][8];
		initAbility( titan, true); // set ability panel of titan
		initAbility( player, false); // set ability panel of player
		insPanel.add(defend);
		insPanel.add(item);
		insPanel.add(ability);
		insPanel.add(runaway);
		add(insPanel, BorderLayout.SOUTH);
	}
	private void initChessboard(){
		int i, j;
		for(i=0;i<10;i++){
			for(j=0;j<10;j++){
				chessButton[i][j] = new JButton("  ");
				chessIcon[i][j] = new JLabel();
				chessIcon[i][j].setHorizontalAlignment( JLabel.CENTER ); // alignment center
				chessIcon[i][j].setPreferredSize( new Dimension(42,25) ); // set label size
				chessIcon[i][j].setBorder( LineBorder.createGrayLineBorder() ); // set label border line
				chessIcon[i][j].setOpaque(true);
				chessIcon[i][j].setBackground(Color.ORANGE);
				gridCons.gridx = j;
				gridCons.gridy = i;
				if( currentBoard.p[i][j]==1 ){//initial black chess
					chessIcon[i][j].setIcon(black);
					chessPanel.add(chessIcon[i][j], gridCons);
				}
				else if( currentBoard.p[i][j]==-1 ){//initial white chess
					chessIcon[i][j].setIcon(white);
					chessPanel.add(chessIcon[i][j], gridCons);
				}
				else if(i>0 && i<9 && j>0 && j<9){ // chessboard
					chessButton[i][j].setActionCommand( Integer.toString(i) + " " + Integer.toString(j) );//set the coordinate for button
					chessButton[i][j].addActionListener( new ChessAction() );
					//chessButton[i][j].setOpaque(false); // let button be transparant
					chessButton[i][j].setBackground(Color.ORANGE); // trigger the transparent effect
					chessPanel.add(chessButton[i][j], gridCons);
				}
				else chessPanel.add(chessButton[i][j], gridCons); // edge
			}
		}
	}
	private void initCharInfo(character ch, JPanel panel, JTextField textHP, JTextField textMP){
                panel.setLayout( new BorderLayout() );

                JLabel head = new JLabel( ch.head );// head picture
                panel.add( head, BorderLayout.NORTH );

                JPanel HPMPPanel = new JPanel();// panel to put the information of hp and mp
                HPMPPanel.setLayout(new GridLayout(2,2));
                JLabel hp = new JLabel("HP:");
                HPMPPanel.add(hp);
		//textHP = new JTextField( ch.getHP() + "/" + ch.getMaxHP() );
		textHP.setEditable( false );
                HPMPPanel.add(textHP);
                JLabel mp = new JLabel("MP:");
                HPMPPanel.add(mp);
                //textMP = new JTextField( ch.getMP() + "/" + ch.getMaxMP() );
                textMP.setEditable(false);
                HPMPPanel.add(textMP);
                panel.add(HPMPPanel, BorderLayout.CENTER);
	}
	private void initItem(){
		item = player.getItem();
		itemPanel = new JPanel();
		itemPanel.setLayout( new BorderLayout() );
		itemPanel.setPreferredSize(new Dimension(100, 150));
		itemMessage = new JTextArea("Nothing\nWhich item you want to use?", 2, 10);
		itemMessage.setEditable( false );
		itemShowing = new JPanel();
		itemShowing.setLayout( new GridLayout( ITEM_ROW , ITEM_COL ) );
		//itemShowing.setBackground( Color.YELLOW );
		JButton useButton = new JButton("use");
		useButton.setBackground( Color.PINK );
		useButton.addActionListener( new UseItem() );
		itemInfo = new JPanel();
		itemInfo.setLayout( new BorderLayout() );
		itemInfo.add( itemMessage, BorderLayout.CENTER );
		itemInfo.add( useButton, BorderLayout.EAST );
		itemPanel.add( itemInfo, BorderLayout.SOUTH );
		itemPanel.add( itemShowing, BorderLayout.CENTER );
		JPanel[] eachItem = new JPanel[ 9/*Item.ITEM_NUM*/ ];
		itemButton = new JButton[ ITEM_ROW ][ ITEM_COL ];
		itemNum = new JLabel[ ITEM_ROW ][ ITEM_COL ];
		int i, j, count;
		count = 0;
		for( i = 0 ; i < ITEM_ROW ; i ++ ){
			for( j = 0 ; j < ITEM_COL ; j ++ ){
				while( count<9 && item[count].getNum()<=0 ) count++;
				if( count >= 9 ){ // nothing else item, let list be empty
					itemButton[i][j] = new JButton();
					itemNum[i][j] = new JLabel();
					itemShowing.add(itemNum[i][j]);
					continue;
				}
				itemButton[i][j] = new JButton( (count<9) ? item[count].getName() : "" );
				itemButton[i][j].setBorderPainted(false); // hide border
				itemButton[i][j].setOpaque(false); // let button be transparant
				itemButton[i][j].setBackground( Color.BLUE ); // let transparant effect active
				itemButton[i][j].addActionListener( new UseItem() );
				itemButton[i][j].setActionCommand( Integer.toString(count) );
				itemNum[i][j] = new JLabel( (count<9) ? Integer.toString( item[count].getNum() ) : "" );
				eachItem[count] = new JPanel();
				eachItem[count].setLayout( new FlowLayout() );
				eachItem[count].add( itemButton[i][j] );
				eachItem[count].add( itemNum[i][j] );
				itemShowing.add( eachItem[count] );
				count++;
			}
		}
		currentItem = -1; // player choose nothing
	}
	public void updateItem(){
		int i, j, count;
		count = 0;
		for( i = 0 ; i < ITEM_ROW ; i ++ ){
			for( j = 0 ; j < ITEM_COL ; j ++ ){
				while( count<9 && item[count].getNum()<=0 ) count++; // empty item
				if( count >= 9 ){ // nothing else item
					itemButton[i][j].setText("");
					itemNum[i][j].setText("");
					continue;
				}
				itemButton[i][j].setText( item[count].getName() );
				itemButton[i][j].setActionCommand( Integer.toString(count) );
				itemNum[i][j].setText( Integer.toString( item[count].getNum() ) );
				count++;
			}
		}
	}
	private void initAbility( character ch, boolean titan){
		int c = titan ? 0 : 1;
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(4,2));
		JPanel[] eachAbi = new JPanel[8];
		JLabel[] abiTitle = new JLabel[8];
		abiTitle[0] = new JLabel("Name: ");
		charAbi[c][0] = new JLabel( ch.getName() );
		abiTitle[1] = new JLabel("LV: ");
		charAbi[c][1] = new JLabel( Integer.toString( ch.getLV() ) );
		abiTitle[2] = new JLabel("HP: ");
		charAbi[c][2] = new JLabel( Integer.toString( ch.getMaxHP() ) );
		abiTitle[3] = new JLabel("MP: ");
		charAbi[c][3] = new JLabel( Integer.toString( ch.getMaxMP() ) );
		abiTitle[4] = new JLabel("ATK: ");
		charAbi[c][4] = new JLabel( Integer.toString( ch.getATK() ) );
		abiTitle[5] = new JLabel("DEF: ");
		charAbi[c][5] = new JLabel( Integer.toString( ch.getDEF() ) );
		abiTitle[6] = new JLabel("LUK: ");
		charAbi[c][6] = new JLabel( Integer.toString( ch.getLUK() ) );
		abiTitle[7] = new JLabel("EXP: ");
		charAbi[c][7] = new JLabel( Integer.toString( ch.getEXP() ) );
		for( int i = 0 ; i < 8 ; i++ ){
			eachAbi[i] = new JPanel();
			eachAbi[i].setLayout( new FlowLayout(FlowLayout.LEFT) );
			eachAbi[i].add( abiTitle[i] );
			eachAbi[i].add( charAbi[c][i] );
			panel.add( eachAbi[i] );
		}
		abilityPanel.add( panel, titan ? BorderLayout.EAST : BorderLayout.WEST );
	}
	public int attackResult(character attacker, character tar, int numFromAttack, int dirCount){
		int value = attacker.getATK() - tar.getDEF();
		if( value <=0 ) value = 1;
		int lossHP = numFromAttack * dirCount * value;
		Random rand = new Random();
		int crit = rand.nextInt(100) + 1;
		if( crit<= attacker.getLUK() ){
			lossHP *= 2;
			showing.setText( showing.getText() + "Crit! ");
		}
		//System.out.println("value=" + value + " num=" + numFromAttack + " dir=" + dirCount + " loss=" + lossHP);
		int remainHP = tar.subHP(lossHP);
		showing.setText( showing.getText() + tar.getName() + " loses HP " + lossHP + "\n");
		updateInfo( tar, remainHP, -1);
		return remainHP;
	}
	public void setSpecialChess(){
		isSpecialChess = true;
	}
	public void updateInfo(character tar, int hp, int mp){
		if( tar instanceof mainCharacter ){
			if( hp>=0 ) playerHP.setText( hp + "/" + player.getMaxHP() );
			if( mp>=0 ) playerMP.setText( mp + "/" + player.getMaxMP() );
			if( hp == 0 ) showing.setText( player.getName() + " lose!\n");
		}
		else{
			if( hp>=0 ) titanHP.setText( hp + "/" + titan.getMaxHP() );
			if( mp>=0 ) titanMP.setText( mp + "/" + titan.getMaxMP() );
			if( hp == 0 ) showing.setText( player.getName() + " win!\n");
		}
	}
	public void updateAbi( boolean titan, int num, int val){
		int c = titan ? 0 : 1 ;
		charAbi[c][num].setText( Integer.toString( val ) );
		charAbi[c][num].setForeground( Color.RED );
		//abilityPanel.repaint();
	}
	public boolean isOver(){
		if( currentBoard.getEmptyCount()==0 ){
			if( currentBoard.getBlackCount() >= currentBoard.getWhiteCount() ){
				player.addMoney(titan.getMoney());
				map.EndBattle( 1 , player.addEXP( titan.getEXP() ), titan ); // win
			}
			else map.EndBattle( 0, 0, titan); // lose
		}
		else if( currentBoard.getWhiteCount()==0 || titan.getHP()==0 ){
			player.addMoney(titan.getMoney());
			map.EndBattle( 1 , player.addEXP( titan.getEXP() ), titan ); // win
		}
		else if( currentBoard.getBlackCount()==0 || player.getHP()==0 )
			map.EndBattle( 0, 0, titan); // lose
		else return false; // the game hasn't been over yet
		return true;
	}
	private void goAction( Move step , boolean isPlayerTurn ){
		int beforeCount, afterCount, dir;
		beforeCount = (color==1) ? currentBoard.getBlackCount() : currentBoard.getWhiteCount();
		dir = eatChess( step );
		currentBoard.makeMove(color, step.x, step.y);
		afterCount = (color==1) ? currentBoard.getBlackCount() : currentBoard.getWhiteCount();
		if( isPlayerTurn == true ) attackResult(player, titan, afterCount - beforeCount - 1, dir);
		else attackResult(titan, player, afterCount - beforeCount - 1, dir);
	}
	private int eatChess(Move step){// eat chess by change picture and return the direction counts
		gridCons.gridx = step.y;
		gridCons.gridy = step.x;
		int enemy = -color;
		int r, c, dirR, dirC, dirCount;
		dirCount = 0;// to count the direction attacker eats chess at
		/*put the chess*/
		chessPanel.remove(chessButton[step.x][step.y]); // remove button
		if( isSpecialChess==true){
			chessIcon[step.x][step.y].setIcon( new ImageIcon( (color==-1) ? "images/specialwhite.png" : "images/specialblack.png") );
			currentBoard.special[step.x][step.y] = 1;
			isSpecialChess = false;
		}
		else chessIcon[step.x][step.y].setIcon( (color == -1) ? white : black); // replace with icon
		chessPanel.add(chessIcon[step.x][step.y], gridCons);
		for ( dirR = -1; dirR <= 1; dirR++ ){ // eat the enemy's chess
			for ( dirC = -1; dirC <= 1; dirC++ ){
				if ( !(dirR == 0 && dirC == 0) && currentBoard.isDirectionValid( color, step.x, step.y, dirR, dirC )){
					dirCount++;
					r = step.x + dirR;
					c = step.y + dirC;
					while( currentBoard.p[ r ][ c ] == enemy){
						if( currentBoard.special[r][c]==1 ){
							//System.out.println("boom!\n");
							chessIcon[r][c].setIcon( new ImageIcon( (color==-1)?"images/specialwhite.png":"images/specialblack.png") );
							item[7].Explode(titan, this);
							showing.setText( showing.getText() + item[7].getStatus() );
						}
						else chessIcon[r][c].setIcon( (color == -1) ? white : black); // change emeny to itself
						r += dirR;
						c += dirC;
					}
				}
			}
		}
		chessPanel.repaint();
		return dirCount;
	}
	public void changeShowingOrPanel(JPanel panel){
		if( isText == true ){ // TextArea showing open, and then change TextArea showing to Panel
			//scroll.setVisible(false);
			displayPanel.remove( scroll );
			displayPanel.add(panel, BorderLayout.SOUTH);
			currentPanel = panel;
			isText = false;
		}
		else if( isText == false ){ // TextArea showing close
			displayPanel.remove(currentPanel);
			if( currentPanel!=panel ){ // change between panels
				displayPanel.add(panel, BorderLayout.SOUTH);
				currentPanel = panel;
			}
			else{ // change Panel to TextArea showing
				//scroll.setVisible(true);
				displayPanel.add(scroll, BorderLayout.SOUTH);
				isText = true;
			}
		}
		displayPanel.validate();
		displayPanel.repaint();
	}
	public Board getBoard(){ // get the current chessboard
		return currentBoard;
	}
	public void changeColor(){
		color = -color;
	}
	public void run(){ // a thread to run for titan
		try{ // wait 1.2 sec
			Thread.sleep(1200);
		}
		catch(InterruptedException e){
			System.out.println("Fatal Error in thread!\n");
		}
		if( currentBoard.isAnyValidMove( color ) == 0 ){ // titan can't put the valid chess
			showing.setText( showing.getText() + "Titan can't put any chess. It's your turn now!\n");
		}
		else{
			tStep = AI.getBestMove( currentBoard.clone(), titan.getType()*2 , color);// get the next step from AI
			goAction( tStep , false);// titan move the chess
		}
		if( isToxic ){ // player use the item to let titan poison
			item[8].Explode(titan, this);
			showing.setText( showing.getText() + item[8].getStatus() );
		}
		color = -color;// change white to black
		isOver();
	}
	public void startThread(){ // create a thread for titan playing chess
		Thread titanThread = new Thread( this );
		titanThread.start();
	}
	public class ChessAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String actionCommand = e.getActionCommand();
			StringTokenizer tok = new StringTokenizer(actionCommand);
			pStep.x = Integer.parseInt(tok.nextToken());
			pStep.y = Integer.parseInt(tok.nextToken());
			if( pStep.x==0 || pStep.x==10 || pStep.y==0 || pStep.y==10 ) return; // edge
			if( currentBoard.isValidMove(color, pStep.x, pStep.y)==true ){
				goAction( pStep , true);//player move the chess
				color = -color;//change black to white
				if( !isOver() ) startThread();
			}
			else if( currentBoard.isAnyValidMove( color )==0 ){
				showing.setText( showing.getText() + "You can't put any chess now.\n");
			}
		}
	}
	public class UseItem implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String actionCommand = e.getActionCommand();
			if( actionCommand.equals("use") ){
				if( currentItem < 0 ) return;
				//System.out.println("use " + currentItem);
				item[currentItem].use( player, titan, Battle.this);
				showing.setText( showing.getText() + item[currentItem].getStatus() );
				updateItem(); // update the item list
				itemMessage.setText("Nothing\nWhich item you want to use?");
				currentItem = -1;
				changeShowingOrPanel( itemPanel ); // change to showing TextArea
			}
			else{
				currentItem = Integer.parseInt( actionCommand );
				itemMessage.setText( item[currentItem].name + "\n" + item[currentItem].info );
			}
		}
	}
	public class DefendAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			int mp;
			if( ( mp = player.subMP( 30 ) ) != -1 ){ // consume MP 30 to do "Defend" action
				updateInfo( player, -1, mp); // update the GUI infomation
				int originDEF = player.getDEF();
				player.setDEF( (int)(originDEF * 1.5) ); // DEF becomes 1.5 times
				showing.setText( showing.getText() + player.getName() + " Defend! Consume MP 30 and DEF becomes 1.5 times: " + player.getDEF() + "\n");
				color = -color; // titan's turn
				tStep = AI.getBestMove( currentBoard.clone() , titan.getType()*2 , color);// get the next step from AI
				goAction( tStep , false ); // titan put the chess
				color = -color; // player's turn
				player.setDEF( originDEF ); // set to origin DEF
				isOver();
			}
			else{
				showing.setText( showing.getText() + "You can't defend, because your MP is less than 30.\n");
			}

		}
	}
	public class ItemAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			changeShowingOrPanel( itemPanel );
		}
	}
	public class AbilityAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			changeShowingOrPanel( abilityPanel );
		}
	}
	public class RunawayAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			Random rand = new Random();
			int c = rand.nextInt(20);
			if( c < player.getLUK() ){
				map.EndBattle( -1 , 0 , titan); // run away from battle
			}
			else{ // change to titan's turn
				color = -color;
				showing.setText( showing.getText() + "Fail to run away\n");
				startThread();
			}
		}
	}
}
