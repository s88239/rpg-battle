class SCORETABLE{
    int[][] val;
    public SCORETABLE( int a[] ){
	val = new int[10][10];
        for( int i = 0; i < 10 ; i++ ){            
            //val[i] = new int[10];
            for( int j = 0; j < 10; j++ )
                val[ i ][ j ] = a[ i * 10 + j ];
        }
    }
    public void showScoreTable(){
        // for( int i = 0; i <= 9 ; i++ )
        // {
        //     for( int j = 0; j <= 9; j++ )
        //         cout<<val[ i ][ j ]<<"\t";
        //     cout<<endl;
        // }
    }
}

public class AI{
    private static final int minAlpha = -20000000;
    private static final int maxBeta  =  20000000;

    private static int defautlScoreTableArray[  ]={
       0,   0,   0,   0,   0,   0,   0,   0,   0,  0,
       0,  75,  -6,   6,   4,   4,   6,  -6,  75,  0,
       0,  -6, -32,  -7,  -7,  -7,  -7, -32,  -6,  0,
       0,   6,  -7,   0,   0,   0,   0,  -7,   6,  0,
       0,   4,  -7,   0,   0,   0,   0,  -7,   4,  0,
       0,   4,  -7,   0,   0,   0,   0,  -7,   4,  0,
       0,   6,  -7,   0,   0,   0,   0,  -7,   6,  0,
       0,  -6, -32,  -7,  -7,  -7,  -7, -32,  -6,  0,
       0,  75,  -6,   6,   4,   4,   6,  -6,  75,  0,
       0,   0,   0,   0,   0,   0,   0,   0,   0,  0
    };
    private static SCORETABLE AIScoreTable = new SCORETABLE( defautlScoreTableArray );   

    public static Move getBestMove( Board current, int depthLeft, int color ){
        Board newBoard;
        int alpha = -20000000;
        int beta  =  20000000;
        int t;
        int enemy = -color;
        Move BestMove = new Move();
        BestMove.x = -1; BestMove.y = -1;
        for( int i = 1; i <= 8; i++ )
        {
            for( int j = 1; j <= 8; j++ )
            {
                newBoard = current.clone();
                if( !newBoard.isValidMove( color, i, j) )
                    continue;
                newBoard.makeMove( color, i, j );
                t = alphabetaMin( newBoard, enemy, alpha, beta, depthLeft - 1 );
                if( alpha < t )
                {
                    alpha = t;
                    BestMove.x = i;
                    BestMove.y = j;
                }
            }
        }
        return BestMove;
    }

    private static int alphabetaMax( Board current, int color, int alpha, int beta, int depthLeft ){
        int enemy = -color;
        if( depthLeft <= 0 || (current.isAnyValidMove( color ) == 0) )
            return AIevalBoard( current, color );
        Board newBoard = current.clone();
        for( int i = 1; i <= 8; i++ )
        {
            for( int j = 1; j <= 8; j++ )
            {
                if( !newBoard.isValidMove( color, i, j) )
                    continue;
                newBoard = current.clone();
                newBoard.makeMove( color, i, j );
                alpha = Math.max( alpha, alphabetaMin( newBoard, enemy, alpha, beta, depthLeft - 1 ) );
            }
            if( alpha >= beta )
                return alpha;
        }
        return alpha;
    }

    private static int alphabetaMin( Board current, int color, int alpha, int beta, int depthLeft ){
        int enemy = -color;
        if( depthLeft <= 0 || (current.isAnyValidMove( color )==0) )
            return AIevalBoard( current, color );
        Board newBoard = current.clone();
        for( int i = 1; i <= 8; i++ )
        {
            for( int j = 1; j <= 8; j++ )
            {
                if( !newBoard.isValidMove( color, i, j) )
                    continue;
                newBoard = current.clone();
                newBoard.makeMove( color, i, j );
                beta  = Math.min( beta, alphabetaMax( newBoard, enemy, alpha, beta, depthLeft - 1 ) );
                if( alpha >= beta )
                    return beta;
            }
        }
        return beta;
    }

    private static int AIevalBoard( Board current, int color ){
        int score = 0;
        for( int i = 1; i < 9; i++ )
            for( int j = 1; j < 9; j++ )
                score += color * current.p[ i ][ j ] * ( AIScoreTable.val[ i ][ j ] );
        return score;
    }

}
