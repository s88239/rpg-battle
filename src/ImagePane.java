import javax.swing.*;
import java.awt.*;
import java.io.*;
import javax.imageio.ImageIO;

public class ImagePane extends JPanel {

    private Image image;
    
    public ImagePane(String path) {
        try {
            image = ImageIO.read(new File(path));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this);
    }
    

    /*public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.getContentPane().add(new ImagePane("table.jpg"));
        frame.setSize(800, 600);
        frame.setVisible(true);
    }*/
}
