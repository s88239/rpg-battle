import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;

public class canvasLabel extends JLabel
{
	private int nowX, nowY, count, s;
	private double x, y;
	public canvasLabel()
	{
		super();
		nowX = 0;
		nowY = 0;
		x = 0;
		y = 0;
		count = 0;
		s = 0;
	}
	public void drawLine(int nowX, int nowY, int x, int y)
	{
		this.nowX = nowX;
		this.nowY = nowY;
		this.x = (double)x;
		this.y = (double)y;
		repaint();
	}
	public void drawLine(int nowX, int nowY, double x, double y)
	{
		this.nowX = nowX;
		this.nowY = nowY;
		this.x = x;
		 this.y = y;
		 repaint();
	}
	public double getgoalX()
	{
		return x;
	}
	public double getgoalY()
	{
		return y;
	}
	public int addCount()
	{
		count++;
		if(count >= s)return 1;
		else return 0;
	}
	public void setS(int s)
	{
		this.s = s;
		count = 0;
	} 
	public void paintComponent(Graphics g)
	{
		Graphics2D g2d = (Graphics2D)g;
		g2d.draw(new Line2D.Double((double)nowX, (double)nowY, x, y));
	}

}
