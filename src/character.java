import java.awt.*;
import javax.swing.*;
public class character
{
	protected int hp, mp, atk, def, luk, lv, exp, maxHP, maxMP, posX, posY;
	protected int money;
	public ImageIcon head, body;
	public JLabel lbl;
	protected String name;
	public character(String head_path, String body_path)
	{
		head = new ImageIcon(head_path);
		body = new ImageIcon(body_path);
		lbl = new JLabel(body);
	}

	public void setATTR(int hp, int maxHP, int mp, int maxMP, int atk, int def, int luk, int lv, int exp, int posX, int posY, int money)
	{
		this.hp = hp;
		this.maxHP = maxHP;
		this.mp = mp;
		this.maxMP = mp;
		this.atk = atk;
		this.def = def;
		this.luk = luk;
		this.lv = lv;
		this.exp = exp;
		this.posX = posX;
		this.posY = posY;
		this.money = money;
	}

	public int getMoney()
	{
		return money;
	}
	
	public int getPosX()
	{
		return posX;
	}
	public int getPosY()
	{
		return posY;
	}
	public String getName()
	{
		return name;
	}
	public int getHP()
	{
		return hp;
	}
	public int getMaxHP()
	{
		return maxHP;
	}
	public int getMP()
	{
		return mp;
	}
	public int getMaxMP()
	{
		return maxMP;
	}
	public int getATK()
	{
		return atk;
	}
	public int getDEF()
	{
		return def;
	}
	public int getLUK()
	{
		return luk;
	}
	public int getLV()
	{
		return lv;
	}
	public int getEXP()
	{
		return exp;
	}
	public void setMoney(int money)
	{
		this.money = money;
	}
	public void setPosX(int posX)
	{
		this.posX = posX;
	}
	public void setPosY(int posY)
	{
		this.posY = posY;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public void setHP(int hp)
	{
		if(hp >= maxHP) this.hp = maxHP;
		else this.hp = hp;
	}
	public void setMaxHP(int maxHP)
	{
		this.maxHP = maxHP;
	}
	public int subHP(int dhp)
	{
		hp -= dhp;
		if(hp < 0) hp = 0;
		else if(hp > maxHP) hp = maxHP;
		return hp;
	}
	public void setMP(int mp)
	{
		if(mp >= maxMP)this.mp = maxMP;
		else this.mp = mp;
	}
	public void setMaxMP(int maxMP)
	{
		this.maxMP = maxMP;
	}
	public int subMP(int dmp)
	{
		if(dmp > mp)return -1;
		mp -= dmp;
		if(mp > maxMP) mp = maxMP;
		return mp;
	}
	public void setATK(int atk)
	{
		this.atk = atk;
	}
	public void setDEF(int def)
	{
		this.def = def;
	}
	public void setLUK(int luk)
	{
		this.luk = luk;
	}
	public void setlv(int lv)
	{
		this.lv = lv;
	}
	public void setEXP(int exp)
	{
		this.exp = exp;
	}

}
