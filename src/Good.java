import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Good extends JLabel
{
	Item item;
	public Good(Item item)
	{
		super(item.getName());
		this.item = item;
        setOpaque(false);
	}

	public int getNum()
	{
		return item.getNum();
	}
}
