import java.io.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;
public class mainCharacter extends character
{
	public static final int ITEM_NUM = 9;
	private int maxEXP, stage;
	private Item[] items;
	private ImageIcon flying;
	public mainCharacter(String head_path, String body_path, File f)
	{
		super(head_path, body_path);
		items = new Item[ ITEM_NUM ];
		flying = new ImageIcon("images/allen-flying.png");
		//read file
	}
	public mainCharacter(String head_path, String body_path, String name, int hp, int maxHP, int mp, int maxMP, int atk, int def, int luk, int lv, int exp, int maxEXP, int posX, int posY/*, int stage*/, int money)
	{
		super(head_path, body_path);
		flying = new ImageIcon("images/allen-flying.png");
		setName(name);
		setMaxEXP(maxEXP);
		this.stage = stage;
		setATTR(hp, maxHP, mp, maxMP, atk, def, luk, lv, exp, posX, posY, money);
		items = new Item[9];
		items[0] = new RedWater(2);
		items[1] = new CrimsonWater(1);
		items[2] = new BlueWater(2);
		items[3] = new SoyaBeanMilk(1);
		items[4] = new Whetstone(1);
		items[5] = new Testosterone(1);
		items[6] = new Lottery(1);
		items[7] = new Bomb(2);
		items[8] = new NeckCrosshair(1);
	}

	public int addEXP(int dexp)
	{
		int up = 0;
		exp += dexp;
		while(exp >= maxEXP)//level up
		{
			lv++;
			exp -= maxEXP;
			maxEXP += lv*100;
			maxHP += 10;
			hp = maxHP;
			maxMP += 6;
			mp = maxMP;
			up++;
		}

		return up;
	}
	public void addMoney(int mo)
	{
        money += mo;
	}

	public int getMaxEXP()
	{
		return maxEXP;
	}
	public void setMaxEXP(int maxEXP)
	{
		this.maxEXP = maxEXP;
	}
	public Item[] getItem()
	{
		return items;
	}
	public void changeIcon(int i)
	{
		if(i == 0)
		{
			lbl.setIcon(body);
			lbl.setSize(30, 80);
			lbl.repaint();
		}
		else if(i == 1)
		{
			lbl.setIcon(flying);
			lbl.setSize(70, 60);
			lbl.repaint();
		}
	}
}
