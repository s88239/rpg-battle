import java.awt.event.*;
public class AttackListener implements MouseListener
{
	private map m;
	private int i;

	public AttackListener(map m, int i)
	{
		this.m = m;
		this.i = i;
	}

	public void mousePressed(MouseEvent e)
	{
	}

	public void mouseClicked(MouseEvent e)
	{
		m.move_attack(i, e.getX(), e.getY());
	}

	public void mouseReleased(MouseEvent e)
	{
	}

	public void mouseEntered(MouseEvent e)
	{
	}

	public void mouseExited(MouseEvent e)
	{
	}
}

